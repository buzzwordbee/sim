#ifndef MISC_H
#include "misc.h"
#endif

#include <string>
#include <boost/lexical_cast.hpp>

using namespace std;


void dictAddSampleWords(Dict& d, int n_words) {
  for (int r = 0; r < n_words; ++r) {    
	string s("w" + boost::lexical_cast<string>(r + 1));    
    d.addWord(s);	  
  }
}

void callWords(Card& card, int z) {
  int row, col;
  for (int r = 0; r < z; ++r) {
    row = rand() % 5;
    col = rand() % 5;
    card.callWord(col, row);
  }
}
