#ifndef DICT_H
#define DICT_H

#include <set>
#include <vector>
#include <string>
#include <iostream>

class Dict {
private:
  std::set<std::string> words;

public:
  Dict() {
  }
  
  void addWord(const std::string& word);  
  std::vector<std::string> getVector() const;
  friend std::ostream& operator<< (std::ostream& lhs, Dict const& rhs);
};

#endif
