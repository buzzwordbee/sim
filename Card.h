#ifndef CARD_H
#define CARD_H

#include "Dict.h"

#include <string>
#include <set>
#include <iostream>

class Card {
private:
  static const int rows = 5;  
  static const int cols = 5;  
  std::string grid[rows][cols];
  std::set<std::string> called;
  int bingo_rows[rows];
  int bingo_cols[cols];
  int bingo_diag[2];
  int bingo_corners;

public:
  Card(Dict& dict);
  void showBingos();
  std::string callWord(int col, int row);
  std::string callWord(std::string s);
  std::pair<int, int> findWord(std::string s);
  void showCalledWords();
  bool isBingo() const;
  friend std::ostream& operator<< (std::ostream& lhs, Card const& rhs);
};

#endif
