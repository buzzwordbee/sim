#ifndef CARD_H
#include "Card.h"
#endif

#include <vector>
#include <iomanip>
#include <algorithm> // for copy
#include <iterator> // for ostream_iterator

using namespace std;

Card::Card(Dict& dict) {
  for (int r = 0; r < cols; ++r) {
    bingo_rows[r] = bingo_cols[r] = cols;
  }
  bingo_diag[0] = bingo_diag[1] = cols;
  bingo_corners = 4;

  vector<string> words = dict.getVector();
  for (int row = 0; row < rows; ++row) {
    for (int col = 0; col < cols; ++col) {
	  grid[row][col] = words.back();
	  words.pop_back();
    } 
  }
  callWord(2, 2);
}
  
void Card::showBingos() {
  cout << "crnrs: " << bingo_corners << endl;
  for (int row = 0; row < rows; ++row) {
    cout << "rows" << row << ": " << bingo_rows[row] << endl;
  }

  for (int col = 0; col < cols; ++col) {
    cout << "cols" << col << ": " << bingo_cols[col] << endl;    
  }

  cout << "diag1: " << bingo_diag[0] << endl;
  cout << "diag2: " << bingo_diag[1] << endl;
}
  
string Card::callWord(int col, int row) {
  string w = grid[row][col];
  pair<set<string>::iterator, bool> ret;
  ret = called.insert(w);
  if (ret.second) {
    --bingo_rows[row];
    --bingo_cols[col];
    if (col == row) --bingo_diag[0];
    if ((cols-1-col) == row) --bingo_diag[1];
    if (((0 == col) || (cols-1 == col)) && ((0 == row) || (rows-1 == row)))
	  --bingo_corners;
  }
	
  return w;
}
  
void Card::showCalledWords() {
  copy(called.begin(), called.end(), ostream_iterator<string>(cout, "\n"));
}

ostream& operator<< (ostream& lhs, Card const& rhs) {
  set<string>::iterator it;
  string s;

  if (rhs.isBingo()) {
    lhs << "Got Bingo!!\n";
  }
  for (int row = 0; row < Card::rows; ++row) {
	for (int col = 0; col < Card::cols; ++col) {
	  it = rhs.called.find(rhs.grid[row][col]);
	  if (it == rhs.called.end()) {
		s = rhs.grid[row][col];
	  } else {
		s = "[" + rhs.grid[row][col] + "]";
	  }
	  lhs << setw(30) << s;
	}
	lhs << endl;
  }
  return lhs;
}

bool Card::isBingo() const {
  if (!(bingo_corners && bingo_diag[0] && bingo_diag[1])) return true;
  for (int r = 0; r < cols; ++r) {
	if (!(bingo_rows[r] && bingo_cols[r])) return true;
  }
  return false;
}

std::string Card::callWord(std::string s) {
  pair<int, int> ret = findWord(s);
  return callWord(ret.first, ret.second);
}

std::pair<int, int> Card::findWord(std::string s) {
  pair<int, int> ret;

  for (int row = 0; row < Card::rows; ++row) {
	for (int col = 0; col < Card::cols; ++col) {
	  if (grid[row][col] == s) {
		ret.first = col;
		ret.second = row;
  
		return ret;
	  }
	}
  }

  ret.first = 2;
  ret.second = 2;
  return ret;
}
