#include "Game.h"

#include <iostream>
#include <cstdlib>

using namespace std;

int main() {
  int n_repeats = 100;
  double res;
  try {
//    srand(unsigned(time(NULL)));
    srand(666);
//	cout << "n_words,n_cards,n_called,n_repeats,n_bingos\n";
    for (int n_words = 30; n_words < 31; n_words+=2) {
//    for (int n_words = 24; n_words < 50; n_words+=3) {
	  for (int n_cards = 5; n_cards < 50; n_cards+=3) {
//	  for (int n_cards = 10; n_cards < 11; n_cards+=3) {
//		for (int n_called = 20; n_called < 21; n_called+=3) {
		for (int n_called = 4; n_called < 21; n_called+=1) {
//    	  cout << n_words;
	      cout << n_cards;
//	      cout << " " << n_cards;
	      cout << " " << n_called;
//	      cout << "," << n_repeats;
	      int sum = 0;
	      for (int r = 0; r < n_repeats; ++r) {
		    Game game(n_words, n_cards, n_called);
		    sum += game.nBingos();
//          cout << "n cards: " << game.nCards() << endl;
//	        cout << "," << game.nBingos() << endl;
//	        cout << endl;
	      }
	      res = 1.0 * sum / n_repeats; // / n_cards;
	      cout << " " << res << endl;
	    }
 	    cout << endl;
	  }
//	  cout << endl;
	}
  } catch (int e) {
    cout << "ouch! (" << e << ")" << endl;
  }
  return 0;
}
