#ifndef GAME_H
#define GAME_H

#include "Dict.h"
#include "Card.h"
#include <vector>
#include <iostream>

class Game {
private:
  Dict dict;
  std::vector<Card> cards;

public:
  Game(int n_words, int n_cards, int z);
  int nCards() const;
  int nBingos() const;
  friend std::ostream& operator<< (std::ostream& lhs, Game const& rhs);  
};

#endif
