#ifndef DICT_H
#include "Dict.h"
#endif

#include <algorithm> // for copy
#include <iterator> // for ostream_iterator
#include <ext/algorithm> // for random_sample

using namespace std;

void Dict::addWord(const string& word) {
  words.insert(word);
}

vector<string> Dict::getVector() const {
  if (words.size() < 24) throw 666;
  vector<string> out(24);
  random_sample(words.begin(), words.end(), out.begin(), out.end());
  random_shuffle(out.begin(), out.end());
  string mid = out[12];
  out[12] = "**FREE**";
  out.push_back(mid);
  return out;
}

ostream& operator<< (ostream& lhs, Dict const& rhs) {
  copy(rhs.words.begin(), rhs.words.end(), ostream_iterator<string>(lhs, "\n"));
  return lhs;
}
