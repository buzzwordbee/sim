#ifndef MISC_H
#define MISC_H

#include "Card.h"
#include "Dict.h"

void dictAddSampleWords(Dict& d, int n_words);
void callWords(Card& card, int z);

#endif
