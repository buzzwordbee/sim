#ifndef GAME_H
#include "Game.h"
#endif

#ifndef MISC_H
#include "misc.h"
#endif

#include <iterator> // for ostream_iterator

using namespace std;

Game::Game(int n_words, int n_cards, int z) {
  dictAddSampleWords(dict, n_words);

  for (int r = 0; r < n_cards; ++r) {
    Card card(dict);
    callWords(card, z);
    cards.push_back(card);
  }
}

int Game::nCards() const {
  return cards.size();
}

int Game::nBingos() const {
  int sum = 0;

  for (vector<Card>::const_iterator it = cards.begin(); it != cards.end(); ++it) {
    if (it->isBingo()) ++sum;
  }
  return sum;
}

ostream& operator<< (ostream& lhs, Game const& rhs) {
  copy(rhs.cards.begin(), rhs.cards.end(), ostream_iterator<Card>(lhs, "\n"));
  return lhs;
}
