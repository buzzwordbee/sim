CC = g++
CFLAGS = -Wall
LDFLAGS =
#LDFLAGS = -lm

all: myprogram

clean:
	rm *.o myprogram

misc.o : misc.c++ misc.h Card.h Dict.h
	${CC} ${CFLAGS} -c misc.c++

Game.o : Game.c++ Game.h Dict.h Card.h misc.h
	${CC} ${CFLAGS} -c Game.c++

Card.o : Card.c++ Card.h Dict.h
	${CC} ${CFLAGS} -c Card.c++

Dict.o : Dict.c++ Dict.h
	${CC} ${CFLAGS} -c Dict.c++

main.o : main.c++ Game.h
	${CC} ${CFLAGS} -c main.c++

myprogram : main.o Dict.o Card.o Game.o misc.o
	${CC} ${CFLAGS} Card.o Dict.o Game.o misc.o main.o ${LDFLAGS} -o myprogram

